# api gde redis doc

## variables de entorno

Renombrar el archivo `.env-example` a `.env`

``mv .env-example .env``

Variables a editar
* PRODUCCION_USUARIO: usuario de produccion de consulta
* PRODUCCION_PASSWORD: password de produccion de consulta
* PRODUCCION_CONNECTSTRING: string conection de la base de datos oracle de gde de produccion
* PRODUCCION_GEGO_USER: usuario de produccion de escritura GEDO_GED
* PRODUCCION_GEGO_PASSWORD: password de produccion de escritura GEDO_GED

* CAPACITACION_USUARIO
* CAPACITACION_PASSWORD
* CAPACITACION_CONNECTSTRING
* CAPACITACION_GEGO_USER
* CAPACITACION_GEGO_PASSWORD

* EMAIL_SMTP_HOST: host para enviar emails
* EMAIL_SMTP_PORT: puerto
* EMAIL_SMTP_SECURE:
* EMAIL_USER:
* EMAIL_PASSWORD:

* SOLR_PRODUCCION: url Solr de produccion
* CRT_XROAD: path al certificado xroad
* KEY_SSL_XROAD: path a la llave xroad
* XROAD_BASEURL: url xroad
* LOGIN_GDE: endpoint login
* URL_DESCARGAR_GDE: endpoint descarga documento

## Endpoint
* `GET /admin_por_repaticion`
* `GET /reparticiones/:ambiente`
* `GET /obtener_usuarios`
* `GET /tratas_expedientes`
* `GET /actualizar`
* `GET /expedientes`
* `GET /caratuladores`
* `GET /expediente/:anio/:numero`
* `GET /expediente/:nroSade`
* `GET /usuario/:dni`
* `GET expediente-ultima/:id`
* `GET /auditoria/expediente`
* `GET /obtenerExpedientesActivos`
* `GET /descargarDocumento/:nroSade`
* `GET /${EVENTOS.usuarios}`
* `GET /obtenerComunicadosUsuario/:ambiente`
* `GET /comunicarUsuariosExternos/:ambiente`
