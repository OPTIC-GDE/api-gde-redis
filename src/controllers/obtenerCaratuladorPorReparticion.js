import { EVENTOS, PERMISOS } from '../config/constantes';
import { redisClient } from '../config/config';

/**
 * Quien administra cada reparticion
 * @param {*} req 
 * @param {*} res 
 */
export const obtenerCaratulador = async (req, res, next) => {
    try {
        let { reparticion } = req.query;
        let resp = JSON.parse(await redisClient.get(EVENTOS.coreUsuarios));
        let usuarios = resp.response.docs;
        if(reparticion){
            usuarios = usuarios.filter(e => e.codigo_reparticion_original == reparticion)
        }
        usuarios = usuarios.map((usuario) => {
            return {
                usuario: usuario.username,
                reparticion: usuario.codigo_reparticion_original,
                permisos: usuario.permisos,
                sector: usuario.sector_mesa,
                nombre_apellido: usuario.nombre_apellido,
            };
        });
        
        usuarios = usuarios.filter((usuario) => 
             (
                usuario.permisos.includes(PERMISOS.CARATULADOR_INTERNO) ||
                usuario.permisos.includes(PERMISOS.CARATULADOR_EXTERNO)
            ));

        if (usuarios) {
            res.json(usuarios);
        } else {
            res.json(usuarios);
        }
    } catch (error) {
        next({ ...error, msg: "Hubo un error al consultar los datos llame a su administrador" })
    }
}





