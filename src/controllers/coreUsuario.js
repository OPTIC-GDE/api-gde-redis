import { EVENTOS } from '../config/constantes';
import { redisClient } from '../config/config';
import { actualizarCoreUsuario } from '../functions';

/**
 * Quien administra cada reparticion
 * @param {*} req 
 * @param {*} res 
 */
export const obtenerCoreUsuarios = async (req,res,next) => {
    try {
        let resp = await redisClient.get(EVENTOS.coreUsuarios);
        if(resp){
            res.json(JSON.parse(resp));
        } else {
            const respuesta = await actualizarCoreUsuario();
            res.json(respuesta);
        }
    } catch (error) {
        next({...error, msg:"Hubo un error al consultar los datos llame a su administrador"})
    }
}

