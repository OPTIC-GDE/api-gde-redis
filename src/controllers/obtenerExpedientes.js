import { consultarExpedientes } from '../functions'

/**
 * Quien administra cada reparticion
 * @param {*} req 
 * @param {*} res 
 */
export const obtenerExpedientes = async (req, res, next) => {
    try {
        const { fechaDesde, fechaHasta, trata, creador, reparticion, numero, ambiente,descripcion,anio } = req.query;
        const respuesta = await consultarExpedientes({ fechaDesde, fechaHasta,descripcion,anio, trata, creador, reparticion, numero, ambiente });
        res.json(respuesta);
    } catch (error) {
        next(error)
    }
}
