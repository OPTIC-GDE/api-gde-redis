import { obtenerExpedientePorHistorial } from '../functions'

/**
 * Quien administra cada reparticion
 * @param {*} req 
 * @param {*} res 
 */
export const obtenerExpedientesActivos = async (req, res, next) => {
    try {
        const { areaTramitacionActual, reparticionOrigen, fechaHasta,fechaDesde, offset, ambiente } = req.query;
        const respuesta = await obtenerExpedientePorHistorial({ areaTramitacion:areaTramitacionActual,fechaHasta,fechaDesde,offset,ambiente,esUltimoPase:true,reparticionOrigen  });
        res.json(respuesta);
    } catch (error) {
        console.log(error)
        next({ ...error, msg: "Hubo un error al consultar los datos llame a su administrador" })
    }
}
