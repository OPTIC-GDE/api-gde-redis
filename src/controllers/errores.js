/**
 *
 *
 * @param {import('../exceptions/HttpExceptions.js')} errorServer
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
export function errorHandle(errorServer, req, res, next) {
    const fecha = new Date();

    let error = errorServer.error || errorServer;
    let mensajeError = `${fecha.getDate()}/${(fecha.getMonth() + 1)} ${(fecha.getHours() + 1)}:${fecha.getMinutes()}: `
    
    if (error.stack) {
        mensajeError += error.message + "\n " + JSON.stringify(error.stack)
        console.error((mensajeError))
    } else {
        mensajeError += "\n " + JSON.stringify(error)
        console.error((mensajeError));
    }
    const status = errorServer.status || 500;
    res.status(status).json({msg: error.message});
}


