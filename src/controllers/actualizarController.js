import { 
    actualizarAdminPorReparticion,
    actualizarReparticiones,
    actualizarTratas,
    actualizarUsuarios
} from '../functions'

const actualizarDatos = async (req,res,next) => {
    try {
        await actualizarTratas();
        await actualizarAdminPorReparticion();
        await actualizarReparticiones({ambiente:"cap"});
        await actualizarReparticiones({ambiente:"prod"});
        await actualizarUsuarios("cap");
        await actualizarUsuarios("prod");
        res.json({msg:'Datos actualizados'})
    } catch (error) {
        console.log(error)
        next({...error, msg:"Hubo un error al actualizar datos"})
    }
}
export {actualizarDatos}