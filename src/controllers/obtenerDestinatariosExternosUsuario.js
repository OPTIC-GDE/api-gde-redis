import { notificarEmails, enviarEmail, obtenerDestinatariosExternosUsuario, obtenerUsuarioByJWT } from '../functions';
import { descargarGEDO } from '../service/gdeWS.service';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
export async function obtenerDestinatariosExternosUsuarioController(req, res, next) {
    try {
        const { ambiente } = req.params;
        const authHeader = req.get("Authorization");
        const userData = await obtenerUsuarioByJWT(authHeader)
        const response = await obtenerDestinatariosExternosUsuario(userData, ambiente)
        res.json(response)
    } catch (error) {
        next(error);
    }

}


/**
 * 
 * @param {import('express').Request<ParmDestinatarios,{},Destinatario[],{}>} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
export async function notificarDestinatariosController(req, res, next) {
    try {
        const { ambiente } = req.params;
        const destinatarios = req.body;
        const authorizationHeader = req.get("Authorization");
        const promises = [];
        destinatarios.forEach(destinatario => {

            const { nroSade } = destinatario;

            const promesa = descargarGEDO({ nroSade, ambiente, authorizationHeader }).then(async res => {
                const pdfAsBase64 = res.data.return;
                return enviarEmail(destinatario,pdfAsBase64)
                    .then(() => notificarEmails(destinatario,ambiente))
                    .catch(e => console.error("Error al enviar el emails %s" ,e));
            }).catch(e => console.error("Error al descargar documento %s", e))
            promises.push(promesa)
        })
        await Promise.all(promises)

        console.log("Se envia la respuesta")
        
        res.json({msg:"Peticion de envio de emails realizada"})
    } catch (error) {
        next(error);
    }

}

/**
 * @typedef {import('../config/types').Destinatario} Destinatario
 * @typedef {import('../config/types').ParmDestinatarios} ParmDestinatarios
 * 
 */



