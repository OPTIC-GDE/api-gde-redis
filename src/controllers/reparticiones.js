import { EVENTOS } from '../config/constantes';
import { redisClient } from '../config/config';
import { actualizarReparticiones } from '../functions';
import HttpException from '../exceptions/HttpExceptions';

/**
 * Quien administra cada reparticion
 * @param {import('express').Request} req
 * @param {import('express').Response} res 
 */
export const obtenerReparticiones = async (req, res, next) => {
    const { ambiente } = req.params;
    const { estadoRegistro } = req.query;
    try {
        let resp = await redisClient.get(EVENTOS.reparticiones + ambiente);
        if (resp) {
            resp = JSON.parse(resp);
        } else {
            resp = await actualizarReparticiones({ ambiente });
        }

        if (estadoRegistro != null) {
            if (estadoRegistro != 0 && estadoRegistro != 1) {
                const error = new Error("El estado del registro debe ser 0 o 1");
                throw new HttpException(error, 401);
            }
            resp = resp.filter(e => e.estadoRegistro == estadoRegistro);
        }

        res.json(resp);

    } catch (error) {
        next(error)
    }
}

