import { obtenerAdministradorPorReparticion } from './adminsitradorPorReparticion';
import { obtenerReparticiones } from './reparticiones';
import { obtenerCoreUsuarios } from './coreUsuario';
import { obtenerExpedientes } from './obtenerExpedientes';
import { actualizarDatos } from './actualizarController';
import { obtenerTratas } from './obtenerTratas';
import { obtenerDetalleExpedientePorNumero, obtenerDetalleExpedientePorNumeroSade } from './consultarDetalleExpediente';
import { obtenerCaratulador } from './obtenerCaratuladorPorReparticion';
import { obtenerUsuarioConDNI } from './obtenerUsuario.controller';
import { obtenerAuditoriaController } from './auditoriaExpediente.controller';
import { obtenerExpedientesActivos } from './obtenerExpedienteActivos';
import { descargarDocumentoMarcadoController } from './descargarDocumentoMarcadoController';
import { obtenerUsuariosController} from './ConsultarUsuariosController';
import {obtenerDestinatariosExternosUsuarioController, notificarDestinatariosController} from './obtenerDestinatariosExternosUsuario' 

export {
  descargarDocumentoMarcadoController,
  obtenerTratas,
  obtenerAdministradorPorReparticion,
  obtenerReparticiones,
  obtenerCoreUsuarios,
  actualizarDatos,
  obtenerExpedientes,
  obtenerDetalleExpedientePorNumero,
  obtenerDetalleExpedientePorNumeroSade,
  obtenerCaratulador,
  obtenerUsuarioConDNI,
  obtenerAuditoriaController,
  obtenerExpedientesActivos,
  obtenerUsuariosController,
  obtenerDestinatariosExternosUsuarioController,
  notificarDestinatariosController
};
