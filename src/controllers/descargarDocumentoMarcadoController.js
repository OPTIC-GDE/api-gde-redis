import {descargarGEDO} from '../service/gdeWS.service.js';
import {addWatermarkToPDF} from '../functions/index.js'
/**
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 * @param {import('express').NextFunction} next 
 */
export const descargarDocumentoMarcadoController = async (req,res,next) => {
    try {
        const {nroSade } = req.params; 
        const documentoResponse = await descargarGEDO({nroSade, ambiente:"cap"})
        const nuevoDocumento = await addWatermarkToPDF(documentoResponse.data.return);
        res.json({data:nuevoDocumento});

    } catch (error) {
        console.log(error?.response?.data)
        next(error);
    }
}

