import { descargarGEDO } from '../service/gdeWS.service';


/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
 export async function descargarDocumentoController( req, res, next) {
    try {
        const {nroSade,ambiente} = req.params
        const response = await descargarGEDO(nroSade, ambiente)
        res.json(response.data)
    } catch (error) {
        next(error);
    }

}

