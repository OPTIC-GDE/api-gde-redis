import { obtenerAuditoria } from '../functions';

/**
 * Quien administra cada reparticion
 * @param {*} req 
 * @param {*} res 
 */
export const obtenerAuditoriaController = async (req, res, next) => {
    try {
        const { fechaDesde, fechaHasta, nroSade, reparticionUsuario, usuario, ambiente } = req.query;
        let numeroExpediente, anioExpediente;
        if(nroSade){
            [_, numeroExpediente, anioExpediente] = nroSade.split("-");
            numeroExpediente = Number(numeroExpediente);
        }
        const respuesta = await obtenerAuditoria({ fechaDesde, fechaHasta, anioExpediente, numeroExpediente, reparticionUsuario, usuario, ambiente});
        res.json(respuesta);
    } catch (error) {
        next(error);
    }
}


