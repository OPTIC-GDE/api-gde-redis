import { EVENTOS } from '../config/constantes';
import { redisClient } from '../config/config';
import { actualizarAdminPorReparticion } from '../functions';

/**
 * Quien administra cada reparticion
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 * @param {import('express').NextFunction} next 
 */
const obtenerAdministradorPorReparticion = async (req,res,next) => {
    try {
        let resp = await redisClient.get(EVENTOS.adminPorReparticion);
        if(resp){
            res.json(JSON.parse(resp));
        } else {
            const respuesta = await actualizarAdminPorReparticion();
            res.json(respuesta);
        }
    } catch (error) {
        next(error);
    }
}

export {obtenerAdministradorPorReparticion}
