import { EVENTOS } from '../config/constantes';
import { consultarDetalleExpediente } from '../functions';

/**
 * Quien administra cada reparticion
 * @param {*} req 
 * @param {*} res 
 */
const obtenerDetalleExpedientePorNumero = async (req,res,next) => {
    try {
        const { anio,numero } = req.params;
        const {ambiente} = req.query;
        const respuesta = await consultarDetalleExpediente({anio,numero,ambiente});
        res.json(respuesta);
    } catch (error) {
        next(error)
    }
}

const obtenerDetalleExpedientePorNumeroSade = async (req,res,next) => {
    try {
        const { nroSade } = req.params;
        const {ambiente} = req.query;
        const respuesta = await consultarDetalleExpediente({numeroSade:nroSade,ambiente});
        res.json(respuesta);
    } catch (error) {
        next(error)
    }
}

export {
    obtenerDetalleExpedientePorNumero,
    obtenerDetalleExpedientePorNumeroSade
}
