import { EVENTOS } from '../config/constantes.js';
import { redisClient } from '../config/config.js';
import { actualizarUsuarios } from '../functions/index.js';
import HttpException from '../exceptions/HttpExceptions.js';
import { nestedInclude } from '../util/util.js';

/**
 * Quien administra cada reparticion
 * @param {import('express').Request} req 
 * @param {import('express').Response} res 
 * @param {import('express').NextFunction} next 
 */
export const obtenerUsuariosController = async (req, res, next) => {
    try {
        let { cuit, nombre_apellido, offset = 0, limit = 300, ambiente = "cap" } = req.query

        if (limit > 300) limit = 300;
        if (limit < 0) limit = 1
        if (offset < 0) offset = 1

        limit = Number(limit);
        offset = Number(offset)

        /** @type {UserData[]} */
        let usuarios = JSON.parse(await redisClient.get(EVENTOS.usuarios + ":" + ambiente));

        /** @type {UserData[]} */
        let usuariosFiltrados;
        let usuariosResponse = [];
        if (!usuarios) usuarios = await actualizarUsuarios(ambiente);

        if (cuit) {
            usuariosFiltrados = usuarios.filter(user => user.cuit === cuit)
        } else if (nombre_apellido) {
            usuariosFiltrados = usuarios.filter(user => {
                const nombres = user.nombre_apellido.toLowerCase().split(' ');
                nombres.push(user.usuario)
                const filtros = String(nombre_apellido).toLowerCase().split(' ');
                return filtros.every((el) => nestedInclude(el, nombres))
            })
        } else {
            usuariosFiltrados = usuarios
        }
        const cantidadTotal = usuariosFiltrados.length;
        const firstElement = offset * limit;
        const lastElement = (firstElement + limit) > cantidadTotal ? cantidadTotal : firstElement + limit;
        for (let i = firstElement; i < lastElement; i++) {
            const user = usuariosFiltrados[i];
            usuariosResponse.push(user)
        }

        res.json({ cantidadTotal, usuarios: usuariosResponse });
    } catch (error) {
        next(error);
    }
}

/**
 * @typedef {import('../config/types').UserData} UserData
 * @typedef {import('../config/types').Destinatario} Destinatario
 * 
 */