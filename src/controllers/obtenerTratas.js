import { EVENTOS } from '../config/constantes';
import { actualizarTratas } from '../functions';
import { redisClient } from '../config/config';

export const obtenerTratas = async (req, res, next) => {
    try {
        let resp = await redisClient.get(EVENTOS.tratasExpedientes);
        if (resp) {
            res.json(JSON.parse(resp));
        } else {
            const respuesta = await actualizarTratas();
            res.json(respuesta);
        }
    } catch (error) {
        next({ ...error, msg: "Hubo un error al consultar los datos llame a su administrador" })
    }
}

