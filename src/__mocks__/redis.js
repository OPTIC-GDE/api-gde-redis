let client;
class redis {
    constructor() {
        this.data = [];
    }

    static createClient() {
        if (!client) {
            client = new redis();
        }
        return client;
    }

    set(key, value, callback) {
        if (this.data.find(e => e.key === key)) {
            this.data.find(e => e.key === key).value = value;
        } else {
            this.data.push({
                key,
                value
            })
        }

        let reply = true;
        let err = null;
        callback(err, reply);
    }

    get(key, callback) {
        let elem = this.data.find(e => e.key === key);
        let reply;
        if (elem) {
            reply = JSON.stringify(elem.value);
        }
        let err = null;
        callback(err, reply);
    }

    clean() {
        this.data = [];
    }
}

module.exports = redis;