/*const request = require('supertest');
const app = require('../app');
const redis = require('redis');
const { EVENTOS } = require('../config/constantes');
const { oracleDbConfig } = require("../config");
const oracledb = require('oracledb');

let client = redis.createClient();
let connection;

describe('GET /reparticiones', () => {
    beforeEach(async () => {
        connection = await oracledb.getConnection(oracleDbConfig);
    });

    afterEach(() => {
        client.clean();
        connection.clean();
    });

    it('deberia obtener informacion precargada en la base de Redis', done => {
        let valor = [{
            id: 1,
            codigo: 123,
            nombre: "nombre",
        }];
        client.set(EVENTOS.reparticiones, JSON.stringify(valor), (err, reply) => { })

        request(app)
            .get(`/consultas/${EVENTOS.reparticiones}`)
            .expect(200, done)
            .expect((res) => {
                expect(JSON.parse(res.body)).toStrictEqual(valor)
            })
            .expect("Content-Type", /json/)
    });

    it('deberia buscar informacion en la base de oracle y luego enviarla', async done => {
        client.get(EVENTOS.reparticiones, (err, reply) => {
            expect(reply).not.toBeDefined();
        })
        await connection.save([
            1, // id
            123, // codigo
            "nombre", // nombre
        ]);

        request(app)
            .get(`/consultas/${EVENTOS.reparticiones}`)
            .expect(200, done)
            .expect((res) => {
                client.get(EVENTOS.reparticiones, (err, reply) => {
                    expect(reply).toBeDefined();
                });
                expect(JSON.parse(JSON.stringify(res.body))).toStrictEqual([{
                    id: 1,
                    codigo: 123,
                    nombre: "nombre",
                }])
            })
            .expect("Content-Type", /json/)
    });
});*/