/*const request = require('supertest');
const app = require('../app');
const redis = require('redis');
const { EVENTOS } = require('../config/constantes');
const oracledb = require('oracledb');
const { oracleDbConfig } = require("../config");

let client = redis.createClient();
let connection;

describe('GET /admin_por_repaticion', () => {
    beforeEach(async () => {
        connection = await oracledb.getConnection(oracleDbConfig);
    });

    afterEach(() => {
        client.clean();
        connection.clean();
    });

    it('deberia obtener informacion precargada en la base de Redis', done => {
        let valor = [{
            nombreUsuario: "nombre",
            apellidoNombre: "apellido",
            reparticiones: [{
                codigo: 123,
                nombre: "reparticion"
            }]
        }]
        client.set(EVENTOS.adminPorReparticion, JSON.stringify(valor), (err, reply) => { })

        request(app)
            .get(`/consultas/${EVENTOS.adminPorReparticion}`)
            .expect(200, done)
            .expect((res) => {
                expect(JSON.parse(res.body)).toStrictEqual(valor)
            })
            .expect("Content-Type", /json/)
    });

    it('deberia buscar informacion en la base de oracle y luego enviarla', async done => {
        client.get(EVENTOS.adminPorReparticion, (err, reply) => {
            expect(reply).not.toBeDefined();
        })

        await connection.save([
            "nombre", // nombreUsuario
            123, // reparticion codigo
            "reparticion", // reparticiones nombre
            "apellido", // apellidoNombre
        ]);

        request(app)
            .get(`/consultas/${EVENTOS.adminPorReparticion}`)
            .expect(200, done)
            .expect((res) => {
                client.get(EVENTOS.adminPorReparticion, (err, reply) => {
                    expect(reply).toBeDefined();
                });
                expect(JSON.parse(JSON.stringify(res.body))).toStrictEqual([{
                    nombreUsuario: "nombre",
                    apellidoNombre: "apellido",
                    reparticiones: [{
                        codigo: 123,
                        nombre: "reparticion"
                    }]
                }])
            })
            .expect("Content-Type", /json/)
    });
});*/