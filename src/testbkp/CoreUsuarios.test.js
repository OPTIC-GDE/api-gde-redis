/*const request = require('supertest');
const app = require('../app');
const redis = require('redis');
const { EVENTOS } = require('../config/constantes');
const axios = require('axios');

let client = redis.createClient();
jest.mock('axios');

describe('GET /coreUsuarios', () => {
    afterEach(() => {
        client.clean();
    });

    it('deberia obtener informacion precargada en la base de Redis', done => {
        let valor = [{
            test: "test"
        }]
        client.set(EVENTOS.coreUsuarios, JSON.stringify(valor), (err, reply) => { })

        request(app)
            .get(`/consultas/${EVENTOS.coreUsuarios}`)
            .expect(200, done)
            .expect((res) => {
                expect(JSON.parse(res.body)).toStrictEqual(valor)
            })
            .expect("Content-Type", /json/)
    });

    it('deberia solicitar informacion mediante axios y luego enviarla', async done => {
        client.get(EVENTOS.coreUsuarios, (err, reply) => {
            expect(reply).not.toBeDefined();
        });

        let data = [{
            test: "test"
        }];

        axios.mockImplementationOnce((url) => Promise.resolve({ data }))

        request(app)
            .get(`/consultas/${EVENTOS.coreUsuarios}`)
            .expect(200, done)
            .expect((res) => {
                client.get(EVENTOS.coreUsuarios, (err, reply) => {
                    expect(reply).toBeDefined();
                });
                expect(JSON.parse(JSON.stringify(res.body))).toStrictEqual(data)
            })
            .expect("Content-Type", /json/)
    });
});*/