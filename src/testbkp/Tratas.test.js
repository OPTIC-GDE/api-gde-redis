/*const request = require('supertest');
const app = require('../app');
const redis = require('redis');
const { EVENTOS } = require('../config/constantes');
const oracledb = require('oracledb');
const { oracleDbConfig } = require("../config");

let client = redis.createClient();
let connection;

describe('GET /tratas_expedientes', () => {
    beforeEach(async () => {
        connection = await oracledb.getConnection(oracleDbConfig);
    });

    afterEach(() => {
        client.clean();
        connection.clean();
    });

    it('deberia obtener informacion precargada en la base de Redis', done => {
        let valor = [{
            codigo: 123,
            descripcion: "descripcion",
        }]
        client.set(EVENTOS.tratasExpedientes, JSON.stringify(valor), (err, reply) => { })

        request(app)
            .get(`/consultas/${EVENTOS.tratasExpedientes}`)
            .expect(200, done)
            .expect((res) => {
                expect(JSON.parse(res.body)).toStrictEqual(valor)
            })
            .expect("Content-Type", /json/)
    });

    it('deberia buscar informacion en la base de oracle y luego enviarla', async done => {
        client.get(EVENTOS.tratasExpedientes, (err, reply) => {
            expect(reply).not.toBeDefined();
        })

        await connection.save([
            123, // codigo
            "descripcion", // descripcion
        ]);

        request(app)
            .get(`/consultas/${EVENTOS.tratasExpedientes}`)
            .expect(200, done)
            .expect((res) => {
                client.get(EVENTOS.tratasExpedientes, (err, reply) => {
                    expect(reply).toBeDefined();
                });
                expect(JSON.parse(JSON.stringify(res.body))).toStrictEqual([{
                    codigo: 123,
                    descripcion: "descripcion",
                }])
            })
            .expect("Content-Type", /json/)
    });
});
*/