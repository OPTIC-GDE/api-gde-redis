import HttpException from "../exceptions/HttpExceptions";

/**
 *
 * @param {import("express").Request} req
 * @param { import("express").Response} res
 * @param {import("express").NextFunction} next
 */
function checkIp(req, res, next) {
	let ip = req.headers['x-forwarded-for'] || req?.connection?.remoteAddress;
	let { ambiente } = req.query;
	if (ambiente) ambiente = ambiente.toLowerCase();
	if (ip == '192.168.24.171') {
		next();
	} else if (ambiente == 'cap' && !req.headers['x-real-ip']) {
		next();
	} else {
		const error = new HttpException(new Error("No esta autorizado para acceder a este recurso"), 403);
		next(error);
	}

}

export {
	checkIp
};

