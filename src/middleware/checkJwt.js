import { AxiosError } from 'axios';
import { axiosIntance } from '../config/config';
import { ValidationError } from '../exceptions/HttpExceptions';
import { parseJwt } from '../util/util';

/**
 *
 * @param {import("express").Request} req
 * @param { import("express").Response} res
 * @param {import("express").NextFunction} next
 */
export function checkJwt(req, res, next) {

    const ambiente = req.params.ambiente || 'cap'
    const authHeader = req.get("Authorization");
    let jwt = '';
    if (!authHeader) {
        const httpError = new ValidationError("Por favor enviar un token valido")
        next(httpError);
        return
    }
    if (authHeader.startsWith('Bearer ')) {
        jwt = authHeader.substring(7, authHeader.length);
    }
    axiosIntance.post("/AUTH_REFRESH_TOKEN/" + ambiente, { jwt })
        .then((response) => {
            let newToken = response?.data?.jwt
            const userToken = parseJwt(newToken);
            if (ambiente.toLocaleLowerCase() === userToken.ambiente) {
                next()
            } else {
                const httpError = new ValidationError("El ambiente del ticket no coincide con el ambiente de la peticion")
                next(httpError);
            }
        })
        .catch(e => {
            if (e instanceof AxiosError) {
                console.log(e.response)
            }
            const httpError = new ValidationError("el token enviado no es valido")
            next(httpError)
        })



}

