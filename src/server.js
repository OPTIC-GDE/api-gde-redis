import { port } from './config/config';
import app from './app.js';

const server = app.listen(port, () => console.log(`La aplicacion se ejecuta en el puerto ${port}`));

export default server;