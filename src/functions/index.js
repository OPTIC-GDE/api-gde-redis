import actualizarCoreUsuario from './usuarios/actualizarCoreUsuario.js';
import actualizarUsuarios from "./usuarios/actualizarUsuario.js" 
import obtenerUsuarioByJWT  from './usuarios/obtenerUsuarioByJWT.js';
import consultarUsuarioDNI from './usuarios/obtenerUsuarioConDNI.js';

import {consultarExpedientes,consultarDetalleExpediente,obtenerExpedientePorHistorial} from './expedientes';

import enviarEmail from './enviarEmail.js'
import actualizarTratas from './actualizarTratas';
import actualizarAdminPorReparticion from './actualizarAdminPorReparticion';
import actualizarReparticiones from './actualizarReparticiones';
import obtenerAuditoria from './obtenerAuditoria.js';
import addWatermarkToPDF from './watermark.js';
import {obtenerDestinatariosExternosUsuario, notificarEmails}  from './obtenerDestinatariosExternosUsuario.js';

export {
  actualizarTratas,
  actualizarAdminPorReparticion,
  actualizarReparticiones,
  actualizarCoreUsuario,
  consultarExpedientes,
  consultarDetalleExpediente,
  consultarUsuarioDNI,
  obtenerAuditoria,
  obtenerExpedientePorHistorial,
  actualizarUsuarios,
  addWatermarkToPDF,
  enviarEmail,
  obtenerDestinatariosExternosUsuario,
  obtenerUsuarioByJWT,
  notificarEmails
};

