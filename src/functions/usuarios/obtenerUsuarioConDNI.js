import oracledb from 'oracledb';
import { getConexion } from "../../config/config";
import HttpException from '../../exceptions/HttpExceptions';

async function obtenerUsuario({dni , ambiente = "prod"}) {
    let connection = null;
    try {
        const query = ` SELECT USUARIO
                        FROM CO_GED.DATOS_USUARIO du
                WHERE SUBSTR(NUMERO_CUIT,3,LENGTH(NUMERO_CUIT) - 3 ) like :dni`;
        connection = await oracledb.getPool(ambiente).getConnection();
        const queryResp = await connection.execute(query,{dni});
        if (queryResp.rows.length < 1) {
            throw new HttpException(new Error("El usuario de DNI: " + dni  +" no existe en GDE el ambiente: " + ambiente ), 404);
        }
        const usuario = queryResp.rows[0][0];
        return {usuario};
    } catch (error) {
        throw  error;
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                throw  err ;
            }
        }
    }
}
export default obtenerUsuario