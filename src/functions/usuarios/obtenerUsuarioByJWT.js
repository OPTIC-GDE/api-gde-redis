import { redisClient } from "../../config/config";
import { EVENTOS } from "../../config/constantes";
import { parseJwt } from "../../util/util";

/**
 * 
 * @param {string} authHeader
 * @returns {Promise<UserData>} 
 */
async function obtenerUsuarioByJWT(authHeader) {
    let jwt = '';
    if (authHeader.startsWith('Bearer ')) {
        jwt = authHeader.substring(7, authHeader.length);
    }
    const jwtData = parseJwt(jwt);
    let userData = await redisClient.get(`${EVENTOS.usuarios}:${jwtData.ambiente}:${jwtData.sub}`)
    userData = JSON.parse(userData);
    return {...userData, token:jwt}

}

/**
 * @typedef {import('../config/types').UserData} UserData
 * 
 */


export default obtenerUsuarioByJWT;