import { CronJob } from 'cron';
import {
    actualizarAdminPorReparticion,
    actualizarReparticiones,
    actualizarTratas,
    actualizarUsuarios
} from './index.js';

let job;
/**
 * Se crea un cron que se ejecuta cada 4 horas,
 * Cada * representa lo siguiente
 * segundos|minutos|horas|dias|meses|anios
 * para leer mas sobre el patron http://crontab.org/
 */
function getInstance() {
    if (!job) {
        job = new CronJob('* * 4 * * *', async function () {
            try {
                await actualizarTratas();
                await actualizarAdminPorReparticion();
                await actualizarReparticiones({ambiente:"cap"});
                await actualizarReparticiones({ambiente:"prod"});
                await actualizarUsuarios("cap");
                await actualizarUsuarios("prod");
            } catch (error) {
                console.error("Error a actualizar los datos con el cron");
                console.error(error)
            }
        }, null, true);
    }

    return job;
}

export default getInstance();

