import oracledb from 'oracledb';
import { EVENTOS } from '../config/constantes';
import { redisClient } from '../config/config';


async function actualizarTratas(ambiente ="prod") {
    let connection = null;
    try {
        const query = `SELECT 
                r.CODIGO_TRATA, 
                r.DESCRIPCION 
            FROM EE_GED.TRATA r`;

        connection = await oracledb.getPool(ambiente).getConnection();
        let res = await connection.execute(query);
        let tratas = [];
        tratas = res.rows.map(tupla => {
            return ({ codigo: tupla[0], descripcion: tupla[1] });
        });
        redisClient.set(EVENTOS.tratasExpedientes, JSON.stringify(tratas));
        return tratas;
    } catch (error) {
        next(error);
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                next(err);
            }
        }
    }
}

export default actualizarTratas
