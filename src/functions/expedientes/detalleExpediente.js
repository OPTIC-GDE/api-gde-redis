import oracledb from 'oracledb';
import { getConexion } from "../../config/config";
import HttpException from '../../exceptions/HttpExceptions';

/*
    codigoSadeArray[0] = 'EX'
    codigoSadeArray[1] = ANIO EXPEDIENTE
    codigoSadeArray[2] = NUMERO EXPEDIENTE
    codigoSadeArray[3] = "" 
    codigoSadeArray[4] = 'NEU'
    codigoSadeArray[5] = REPARTICION DEL EXPEDIENTE
*/

/**
 * 
 * @param {number} anio anio del expediemte
 * @param {number} numero numero del expediente
 * @param {string} numeroSade numero sade del expediente
 * @param {string} ambiente ambiente del expediente
 */
async function detalleExpediente({ anio, numero, numeroSade, ambiente = "prod" }) {
    let connection = null;
    try {
        let anioParam = anio;
        let numeroParam = numero;

        if (numeroSade) {
            const codigoSadeArray = numeroSade.split("-")
            anioParam = codigoSadeArray[1];
            numeroParam = codigoSadeArray[2];
        }
        numeroParam = Number(numeroParam);

        const columnasDetalle = [
            "ee.fecha_creacion",
            "ee.fecha_modificacion",
            "ee.descripcion",
            "ee.usuario_creador",
            "ee.tipo_documento",
            "ee.id_workflow",
            "ee.anio",
            "ee.numero",
            "ee.codigo_reparticion_actuacion",
            "ee.codigo_reparticion_usuario",
            "ee.solicitud_iniciadora",
            "ee.estado",
            "ee.sistema_creador",
            "t.assignee_ as assignee_ee",
            "t.codigo_trata AS codigo_trata"]

        const columnasDocumentoInfo = [
            "d.numero_Sade",
            "d.motivo",
            "d.fecha_creacion as fecha_creacion_documento",
            "d.fecha_asociacion",
            "ed.posicion"
        ]

        const columnasHistorialPases = [
            "h.ord_hist",
            "h.fecha_operacion as fecha_pase",
            "h.codigo_reparticion_destino",
            "h.motivo",
            "h.descripcion_reparticion_origen",
            "h.descripcion_reparticion_destin",
            "h.reparticion_usuario as codigo_reparticion_origen",
            "h.usuario as remitente",
            "h.destinatario as destinatario",
            "h.TIPO_OPERACION"
        ];

        const query = `--sql
        SELECT  ${columnasDetalle.join(", ")}, 
                ${columnasDocumentoInfo.join(", ")}, 
                ${columnasHistorialPases.join(", ")}
        FROM EE_GED.EE_EXPEDIENTE_ELECTRONICO ee 
            LEFT JOIN EE_GED.JBPM4_TASK t ON ee.ID_WORKFLOW = t.EXECUTION_ID_ 
            INNER JOIN EE_GED.TRATA t ON ee.id_trata = t.id
            INNER JOIN ee_ged.EE_EXPEDIENTE_DOCUMENTOS ed  ON ee.ID = ed.ID
            INNER JOIN ee_ged.DOCUMENTO d  ON d.ID = ed.ID_DOCUMENTO
            INNER JOIN ee_ged.HISTORIALOPERACION h ON h.ID_EXPEDIENTE = ee.ID
        WHERE ee.anio = :anio AND ee.numero = :numero 
        ORDER BY ee.FECHA_CREACION DESC, ed.posicion ASC, h.ord_hist ASC`;

        let pars = { anio: anioParam, numero: numeroParam }
        connection = await oracledb.getPool(ambiente).getConnection();
        let queryResp = await connection.execute(query, pars);

        if (queryResp.rows.length < 1) {
            throw new HttpException(new Error("El expediente numero: " + numero + " anio:" + anio + " no existe"), 400);
        }
        //Si retorna almenos un expedientes, se carga 
        let expediente = { DOCUMENTOS: [], PASES: [] };

        for (let i = 0; i < columnasDetalle.length; i++) {
            expediente[queryResp.metaData[i].name] = queryResp.rows[0][i];
        }
        //El puntero de la columna es el primer elemento del array y ademas es el codigo sade
        const punteroDocumento = columnasDetalle.length;
        //El puntero de la columna es el primer elemento del array y ademas es el orden de pase
        const punteroPase = columnasDetalle.length + columnasDocumentoInfo.length;
        const punteroFinal = columnasDetalle.length + columnasDocumentoInfo.length + columnasHistorialPases.length;
        
        const documentoSet = new Set();
        const paseSet = new Set();
        queryResp.rows.forEach(e => {

            if (!documentoSet.has(e[punteroDocumento])) {
                documentoSet.add(e[punteroDocumento])
                let newDocumento = {}
                for (let i = punteroDocumento; i < punteroPase; i++) {
                    newDocumento[queryResp.metaData[i].name] = e[i];
                }
                expediente.DOCUMENTOS.push(newDocumento);
            }

            if (!paseSet.has(e[punteroPase])) {
                paseSet.add(e[punteroPase])
                let newPase = {};
                // Recorro el array las columnas de pases
                for (let i = punteroPase; i < punteroFinal; i++) {
                    newPase[queryResp.metaData[i].name] = e[i];
                }
                expediente.PASES.push(newPase);
            }

        });

        return expediente

    } catch (error) {
        throw error;
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                throw err;
            }
        }
    }
}

export default detalleExpediente;
