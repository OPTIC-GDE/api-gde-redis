import consultarExpedientes from "./consultarExpedientes";
import consultarDetalleExpediente from "./detalleExpediente";
import obtenerExpedientePorHistorial from "./obtenerExpedientePorHistorial";

export {
    consultarExpedientes,
    consultarDetalleExpediente,
    obtenerExpedientePorHistorial
}