import { createTransport } from 'nodemailer';

const transporter = createTransport({
    host: process.env.EMAIL_SMTP_HOST,
    port: process.env.EMAIL_SMTP_PORT,
    secure: (process.env.EMAIL_SMTP_SECURE === '1'),
    auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASSWORD

    }
})


/**
 * 
 * @param {import('../config/types').Destinatario} destinatario 
 * @param {string} documento documento pdf base64
 */
async function enviarEmail(destinatario, documento) {
    console.log((process.env.EMAIL_SMTP_SECURE === '1'))
    const { nroSade, reparticionDestino,nombreUsuario } = destinatario;
    const info = await transporter.sendMail({
        from: `"GDE no responder" <${process.env.EMAIL_USER}>`,
        to: reparticionDestino,
        subject: "Tiene una notificacion del sistema GDE",
        text: `Hola ${nombreUsuario},
Tiene una notificacion GDE para leer.
Para leer descargar el documento adjunto nro de documento: ${nroSade}`,
        html:`Hola ${nombreUsuario},
Tiene una notificacion GDE para leer.
Para leer descargar el documento adjunto nro de documento: ${nroSade}`,
        attachments: [{
            filename: nroSade + ".pdf",
            content: documento,
            encoding: "base64"
        }]
    }).catch(e => console.error(e))
    console.log("Message sent to: %s nroSade: %s info: %s", reparticionDestino, nroSade, JSON.stringify(info));

}

export default enviarEmail