import oracledb from 'oracledb';
import { getConexionEdit } from "../config/config"
import { CAMPOS_COMUNICACION, CAMPOS_COMUNICACION_DESTINO, CAMPOS_GEDO_DOCUMENTOS, TABLAS } from "../config/DBscheme";

/**
 *
 *
 * @param {UserData} datosUsuario
 * @param {string} ambiente
 * @returns {Promise<Destinatario[]>}
 */
export async function obtenerDestinatariosExternosUsuario(datosUsuario, ambiente) {
    let conexion;
    try {
        conexion = await oracledb.getPool(ambiente).getConnection();
        const QUERY = `--sql
            SELECT 
                ${CAMPOS_COMUNICACION_DESTINO.ID},
                ${CAMPOS_COMUNICACION_DESTINO.REPARTICION_EXTERNA},
                ${CAMPOS_COMUNICACION_DESTINO.MESA_EXTERNO},
                ${CAMPOS_GEDO_DOCUMENTOS.NUMERO},
                ${CAMPOS_COMUNICACION_DESTINO.LEIDO},
                ${CAMPOS_COMUNICACION_DESTINO.NOMBRE_USUARIO}
            FROM ${TABLAS.COMUNICACION_DESTINO} 
                INNER JOIN ${TABLAS.COMUNICACION} ON 
                    ${CAMPOS_COMUNICACION.ID} = ${CAMPOS_COMUNICACION_DESTINO.ID_COMUNICACION} 
                INNER JOIN  ${TABLAS.GEDO_DOCUMENTOS} ON 
                    ${CAMPOS_GEDO_DOCUMENTOS.ID} = ${CAMPOS_COMUNICACION.ID_DOCUMENTO}
            WHERE ${CAMPOS_COMUNICACION_DESTINO.REPARTICION_EXTERNA} like '%@%'
                AND ${CAMPOS_COMUNICACION_DESTINO.MESA_EXTERNO} like :reparticion
            `
        const result = await conexion.execute(QUERY, {
            reparticion: datosUsuario.reparticion + ' -%'
        })
        const data = result.rows.map(comunicacion => ({
            id: comunicacion[0],
            reparticionDestino: comunicacion[1],
            mesaExterna: comunicacion[2],
            nroSade: comunicacion[3],
            leido: comunicacion[4],
            nombreUsuario: comunicacion[5]
        }))
        return data;

    } catch (error) {
        console.error(error);
        return error;
    } finally {
        if (conexion) {
            try {
                await conexion.close();
            } catch (err) {
                throw err;
            }
        }
    }
}


/**
 * Esta funcion actualiza la tabla de comunicados cuando un emails es enviado con exito
 * @param {Destinatario} destinatario 
 * @param {string} ambiente 
 */
export async function notificarEmails(destinatario, ambiente) {

    if (!destinatario.id) {
        throw new Error("El id del destinatario no puede ser null")
    }

    const QUERY = `--sql
    UPDATE ${TABLAS.COMUNICACION_DESTINO} SET ${CAMPOS_COMUNICACION_DESTINO.LEIDO} = SYSDATE
    WHERE ${CAMPOS_COMUNICACION_DESTINO.ID} = ${destinatario.id}`
    const conexion = await oracledb.getConnection(getConexionEdit(ambiente)).catch(e => console.error("error al obtener datos de conexion"))
    if (!conexion) return
    conexion.execute(QUERY)
        .then(e => { conexion.commit(); console.log(`Actualizado ${JSON.stringify(destinatario)}`) })
        .catch(e => console.error("error al actualizar %s error %s", destinatario, e))
        .finally(() => { console.log("cerrando conexion"); conexion.close() })
}


/**
 * @typedef {import('../config/types').UserData} UserData
 * @typedef {import('../config/types').Destinatario} Destinatario
 * 
 */