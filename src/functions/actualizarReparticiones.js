import oracledb from 'oracledb';
import { EVENTOS } from '../config/constantes';
import { redisClient } from '../config/config';

async function actualizarReparticiones({ ambiente }) {
    let connection = null;
    try {

        const query = `--sql   
                SELECT 
                    CODIGO_REPARTICION, 
                    NOMBRE_REPARTICION, 
                    VIGENCIA_DESDE, 
                    VIGENCIA_HASTA, 
                    ESTADO_REGISTRO 
                FROM TRACK_GED.SADE_REPARTICION r`;

        connection = await oracledb.getPool(ambiente).getConnection();

        let res = await connection.execute(query);
        let reparticiones = [];
        reparticiones = res.rows.map(tupla => {
            return ({ 
                codigoReparticion: tupla[0], 
                nombreReparticion: tupla[1], 
                vigenciaDesde: tupla[2], 
                vigenciaHasta: tupla[3],
                estadoRegistro:tupla[4] });
        });

        redisClient.set(EVENTOS.reparticiones + ambiente, JSON.stringify(reparticiones));
        return reparticiones;
    } catch (error) {
        throw error;
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                throw err;
            }
        }
    }
}

export default actualizarReparticiones
