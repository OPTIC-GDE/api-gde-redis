import oracledb from 'oracledb';
import { getConexion } from "../config";
import {actualizarReparticiones} from '../functions/index.js';
import { redisClient } from '../config/config';


jest.mock('oracledb');
jest.mock('../config');
jest.mock('redis');

describe('actualizarReparticiones', () => {
    it('should update reparticiones successfully', async () => {
        const reparticiones = require('../__mocks__/reparticiones.json');
        // Mocks
        const mockConnection = {
            execute: jest.fn().mockResolvedValue({
                rows: [
                    [1, 'Reparticion 1', '2021-01-01', '2022-01-01', 1],
                    [2, 'Reparticion 2', '2021-01-01', '2022-01-01', 0]]
            }),
            close: jest.fn()
        };
        const mockGetConexion = jest.fn().mockReturnValue({});
        const mockSetRedisData = jest.fn((event, value) => value);


        oracledb.getConnection = jest.fn().mockResolvedValue(mockConnection);
        getConexion.mockImplementation((ambiente) => mockGetConexion(ambiente));
        redisClient.set.mockImplementation((event, value) => mockSetRedisData(event, value));

        // Test
        const ambiente = 'cap';
        const expectedReparticiones = [
            { codigoReparticion: 1, nombreReparticion: 'Reparticion 1', vigenciaDesde: '2021-01-01', vigenciaHasta: '2022-01-01', estadoRegistro: 1 },
            { codigoReparticion: 2, nombreReparticion: 'Reparticion 2', vigenciaDesde: '2021-01-01', vigenciaHasta: '2022-01-01', estadoRegistro: 0 }
        ];

        const result = await actualizarReparticiones({ ambiente });

        expect(mockGetConexion).toHaveBeenCalledWith('test');
        expect(oracledb.getConnection).toHaveBeenCalledWith({});
        expect(mockConnection.execute).toHaveBeenCalledWith(
            expect.any(String)
        );
        expect(mockSetRedisData).toHaveBeenCalledWith('reparticiones' + ambiente, JSON.stringify(expectedReparticiones));
        expect(result).toEqual(expectedReparticiones);
        expect(mockConnection.close).toHaveBeenCalled();
    });

    it('should throw an error if SQL execution fails', async () => {
        // Mocks
        const mockConnection = {
            execute: jest.fn().mockRejectedValue(new Error('SQL execution failed')),
            close: jest.fn()
        };
        oracledb.getConnection = jest.fn().mockResolvedValue(mockConnection);

        // Test
        await expect(actualizarReparticiones({ ambiente: 'cap' })).rejects.toThrowError('SQL execution failed');
        expect(mockConnection.close).toHaveBeenCalled();
    });

    it('should return an empty array if no reparticiones found', async () => {
        // Mocks
        const mockConnection = {
            execute: jest.fn().mockResolvedValue({
                rows: []
            }),
            close: jest.fn()
        };
        oracledb.getConnection = jest.fn().mockResolvedValue(mockConnection);

        // Test
        const result = await actualizarReparticiones({ ambiente: 'cap' });
        expect(result).toEqual([]);
        expect(mockConnection.close).toHaveBeenCalled();
    });

});