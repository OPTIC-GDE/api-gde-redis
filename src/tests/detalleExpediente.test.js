const oracledb = require('oracledb');
const { getConexion } = require("../config");
const {detalleExpediente} = require('../functions');
const HttpException = require('../exceptions/HttpExceptions');

jest.mock('oracledb');
jest.mock('../config');
jest.mock('../exceptions/HttpExceptions');

describe('detalleExpediente', () => {

    it('should return expediente details successfully', async () => {
        // Mock data
        const expedienteResult = require('../__mocks__/detalleExpediente.json');
        const metaDataDetalle = [
            { name: 'FECHA_CREACION' },
            { name: 'FECHA_MODIFICACION' },
            { name: 'DESCRIPCION' },
            { name: 'USUARIO_CREADOR' },
            { name: 'TIPO_DOCUMENTO' },
            { name: 'ID_WORKFLOW' },
            { name: 'ANIO' },
            { name: 'NUMERO' },
            { name: 'CODIGO_REPARTICION_ACTUACION' },
            { name: 'CODIGO_REPARTICION_USUARIO' },
            { name: 'SOLICITUD_INICIADORA' },
            { name: 'ESTADO' },
            { name: 'SISTEMA_CREADOR' },
            { name: 'ASSIGNEE_EE' },
            { name: 'CODIGO_TRATA' },
            { name: 'NUMERO_SADE' },
        ];

        const metaDataDocumentoInfo = [
            { name: 'NUMERO_SADE' },
            { name: 'MOTIVO' },
            { name: 'FECHA_CREACION_DOCUMENTO' },
            { name: 'FECHA_ASOCIACION' },
            { name: 'POSICION' },
        ];

        const metaDataHistorialPases = [
            { name: 'ORD_HIST' },
            { name: 'FECHA_PASE' },
            { name: 'CODIGO_REPARTICION_DESTINO' },
            { name: 'MOTIVO' },
            { name: 'DESCRIPCION_REPARTICION_ORIGEN' },
            { name: 'DESCRIPCION_REPARTICION_DESTIN' },
            { name: 'CODIGO_REPARTICION_ORIGEN' },
            { name: 'REMITENTE' },
            { name: 'DESTINATARIO' },
            { name: 'TIPO_OPERACION' },
        ];

        // Mocks
        const mockConnection = {
            execute: jest.fn((sql,par) => ({
                rows: [Object.values(expedienteResult)],
                metaData: [
                    ...metaDataDetalle,
                    ...metaDataDocumentoInfo,
                    ...metaDataHistorialPases,
                ],
            })),
            close: jest.fn(),
        };

        const mockGetConexion = jest.fn().mockReturnValue({});
        oracledb.getConnection = jest.fn().mockResolvedValue(mockConnection);
        getConexion.mockImplementation(ambiente => mockGetConexion(ambiente));

        // Test
        const params = {
            anio: 2023,
            numero: 991,
            numeroSade: 'EX-2023-00000991--PROVINCIAS-CGP#MEI',
            ambiente: 'cap',
        };

        const result = await detalleExpediente(params);

        expect(mockGetConexion).toHaveBeenCalledWith('cap');
        expect(oracledb.getConnection).toHaveBeenCalledWith({});
        expect(mockConnection.execute).toHaveBeenCalledWith(
            expect.any(String),{
                anio: "2023",
                numero: expect.any(Number),
            }
        );
        console.log(result)
        console.log(expedienteResult)
        expect(result).toEqual(expedienteResult);
        expect(mockConnection.close).toHaveBeenCalled();
    })
});