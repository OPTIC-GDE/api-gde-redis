import oracledb from 'oracledb';
import { getConexion } from "../config";
import {consultarExpedientes} from '../functions/index.js';


jest.mock('oracledb');
jest.mock('../config');


describe('consultarExpedientes', () => {
    it('should return expedientes successfully', async () => {
        const expedientesResult = require('../__mocks__/expedientes.json');


        const metaData = [
            { name: 'FECHA_CREACION' },
            { name: 'FECHA_MODIFICACION' },
            { name: 'DESCRIPCION' },
            { name: 'USUARIO_CREADOR' },
            { name: 'TIPO_DOCUMENTO' },
            { name: 'ID_WORKFLOW' },
            { name: 'ANIO' },
            { name: 'NUMERO' },
            { name: 'CODIGO_REPARTICION_ACTUACION' },
            { name: 'CODIGO_REPARTICION_USUARIO' },
            { name: 'SOLICITUD_INICIADORA' },
            { name: 'ESTADO' },
            { name: 'SISTEMA_CREADOR' },
            { name: 'ASSIGNEE_EE' },
            { name: 'CODIGO_TRATA' }
          ]

        // Mocks
        
        const mockConnection = {
            execute: jest.fn().mockResolvedValue({
                rows: expedientesResult.map(e => Object.values(e)),
                metaData
            }),
            close: jest.fn()
        };

        const mockGetConexion = jest.fn().mockReturnValue({});
        oracledb.getConnection = jest.fn().mockResolvedValue(mockConnection);
        getConexion.mockImplementation((ambiente) => mockGetConexion(ambiente));

        // Test
        const consultaParams = {
            fechaDesde: '01/01/2000',
            descripcion: 'Expediente',
            anio: 2023,
            offset: 0,
            ambiente: 'cap'
        };

        const result = await consultarExpedientes(consultaParams);

        expect(mockGetConexion).toHaveBeenCalledWith('cap');
        expect(oracledb.getConnection).toHaveBeenCalledWith({});
        expect(mockConnection.execute).toHaveBeenCalledWith(expect.any(String), expect.objectContaining({
            fechaDesde: '01/01/2000',
            cantidadRows: 300,
            offset: 0,
            descripcion: 'expediente',
            anio: 2023
        }));
        expect(result).toEqual(expedientesResult);
        expect(mockConnection.close).toHaveBeenCalled();
    });
});