import { ValidationError } from '../exceptions/HttpExceptions';

/**
 * 
 * @param {string} token 
 * @returns {GDEToken} 
 *          Datos del token de GDE 
 */
export function parseJwt(token) {
    return JSON.parse(Buffer.from(token.split('.')[1], 'base64').toString());
}


/**
 * @typedef {import('../config/types').GDEToken} GDEToken 
*/

/**
 *
 *
 * @export
 * @param {string} element
 * @param {string[]} array
 */
export function nestedInclude(element, array) {
    let res = false;
    let i = 0;

    while (!res && i < array.length) {
        res = array[i].includes(element)
        i++;
    }
    return res;

}
/**
 * Valida y divide el codigo  sade en un array
 * @exports @param {string} nroSade //example IF-2024-00005027-PROVINCIAS-OPTPD#SGP
 * @returns {string}
 */
export function validarNroSade(nroSade) {
    const arrayNroSade = nroSade.split("-");
    if (!Number.isInteger(arrayNroSade[1])) throw new ValidationError("El año del documento es inválido")
    if (!Number.isInteger(arrayNroSade[2])) throw new ValidationError("El número de documento es inválido")
    return arrayNroSade;

}
