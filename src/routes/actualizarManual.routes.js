import express from 'express';
import { ROUTES } from '../config/constantes';
import {actualizarDatos} from '../controllers/index';

const app = express.Router();

app.use(ROUTES.actualizar,actualizarDatos)

export default app;
