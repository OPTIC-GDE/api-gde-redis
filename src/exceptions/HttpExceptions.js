/**
 *
 *
 * @class HttpException
 */
class HttpException {
    /**
     * @param {Error} error
     * @param {int} status
     * @memberof HttpException
     */
    constructor(error, status) {
        this.error = error;
        this.status = status;
    }
}

/**
 * @type {HttpStatusType}
 */
export const HttpStatusCode = {
    OK: 200,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    NOT_FOUND: 404,
    INTERNAL_SERVER: 500
}

class BaseError extends Error {

    /**
     * Creates an instance of BaseError.
     * @param {String} name
     * @param {HttpStatusType} httpCode
     * @param {String} description
     * @param {Boolean} isOperational
     * @memberof BaseError
     */
    constructor(name, httpCode, isOperational, description) {
        super(description);
        Object.setPrototypeOf(this, new.target.prototype);

        this.name = name;
        this.httpCode = httpCode;
        this.isOperational = isOperational;

        Error.captureStackTrace(this);
    }
}

export class APIError extends BaseError {
    constructor(name, httpCode = HttpStatusCode.INTERNAL_SERVER, isOperational = true, description = 'internal server error') {
        super(name, httpCode, isOperational, description);
    }
}

export class ValidationError extends APIError {
    constructor(description = "Validation error") {
        super('Validation error', HttpStatusCode.BAD_REQUEST, true, description)
    }
}

export class BadCredentials extends APIError {
    constructor(description = "Bad credentials") {
        super('Bad credential error', HttpStatusCode.UNAUTHORIZED, true, description)
    }
}

/**
 * @typedef {import('../config/types').HttpStatusType} HttpStatusType
 * 
 */

export default HttpException;
