/**
 * @typedef {{ 
*     OK: 200,
*     BAD_REQUEST: 400,
*     UNAUTHORIZED: 401,
*     NOT_FOUND: 404,
*     INTERNAL_SERVER: 500
* }} HttpStatusType
*/

/**
 * @typedef {Object} Destinatario
 * @property  {number} id 
 * @property  {string} reparticionDestino 
 * @property  {string} nombreUsuario 
 * @property  {string} mesaExterna 
 * @property  {string} nroSade 
 * @property  {string} leido       
*/

/**
 * @typedef {Object} ParmDestinatarios
 * @property  {string} ambiente 
*/

/**
 * @typedef {Object} UserData
 * @property  {string} reparticion 
 * @property  {string} nombre_apellido 
 * @property  {string} nombre 
 * @property  {string} apellido 
 * @property  {string} cuit 
 * @property  {string} usuario
 * @property  {string} token
*/


/** 
 * @typedef {Object} GDEToken
 * @property {string} sub - nombre de usuario
 * @property {string} ambiente - ambiente para el que es valido el token
 * @property {number} exp - fecha de expiracion del token en nano segundos
 * @property {number} iat - fecha de emision del toke en nano segundos
 */


/**
* @typedef {object} Expediente
* @property {string} FECHA_CREACION
* @property {string} FECHA_MODIFICACION
* @property {string} DESCRIPCION
* @property {string} USUARIO_CREADOR
* @property {string} TIPO_DOCUMENTO
* @property {string} ID_WORKFLOW
* @property {number} ANIO
* @property {number} NUMERO
* @property {string} CODIGO_REPARTICION_ACTUACION
* @property {string} CODIGO_REPARTICION_USUARIO
* @property {number} SOLICITUD_INICIADORA
* @property {string} ESTADO
* @property {string} SISTEMA_CREADOR
* @property {string} ASSIGNEE_EE
* @property {string} CODIGO_TRATA
* @property {string} NRO_SADE
*/

/**
* @typedef {object} DetalleExpediente
* @property {string} FECHA_CREACION
* @property {string} FECHA_MODIFICACION
* @property {string} DESCRIPCION
* @property {string} USUARIO_CREADOR
* @property {string} TIPO_DOCUMENTO
* @property {string} ID_WORKFLOW
* @property {number} ANIO
* @property {number} NUMERO
* @property {string} CODIGO_REPARTICION_ACTUACION
* @property {string} CODIGO_REPARTICION_USUARIO
* @property {number} SOLICITUD_INICIADORA
* @property {string} ESTADO
* @property {string} SISTEMA_CREADOR
* @property {string} ASSIGNEE_EE
* @property {string} CODIGO_TRATA
* @property {DOCUMENTOS[]} DOCUMENTOS
* @property {PASES[]} PASES
*/

/**
* @typedef {object} DOCUMENTOS
* @property {string} NUMERO_SADE
* @property {string} MOTIVO
* @property {number} POSICION
* @property {string} FECHA_CREACION_DOCUMENTO
* @property {string} FECHA_ASOCIACION
*/

/**
* @typedef {object} PASES
* @property {number} ORD_HIST
* @property {string} MOTIVO
* @property {string} REMITENTE
* @property {string} DESTINATARIO
* @property {string} CODIGO_REPARTICION_DESTINO
* @property {string} CODIGO_REPARTICION_ORIGEN
* @property {string} DESCRIPCION_REPARTICION_ORIGEN
* @property {string} TIPO_OPERACION
* @property {string} FECHA_PASE
* @property {string} DESCRIPCION_REPARTICION_DESTIN
*/


exports.unused = {};
