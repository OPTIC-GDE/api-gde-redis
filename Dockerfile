# 
FROM node:20-alpine
RUN mkdir -p /home/node/api-redis/node_modules && chown -R node:node /home/node/api-redis
WORKDIR /home/node/api-redis
COPY package*.json ./
USER node
RUN npm ci
COPY --chown=node:node . .
EXPOSE 8080
CMD [ "node", "app.js" ]